# Applied upstream

###########################################################################
# Posted and applied
###########################################################################

###########################################################################
# Posted
###########################################################################
crypto-x86-aes-gcm-fix-PREEMPT_RT-issue-in-gcm_crypt.patch

###########################################################################
# John's printk queue
###########################################################################
0001-printk-Add-notation-to-console_srcu-locking.patch
0002-printk-Properly-deal-with-nbcon-consoles-on-seq-init.patch
0003-printk-nbcon-Remove-return-value-for-write_atomic.patch
0004-printk-Check-printk_deferred_enter-_exit-usage.patch
0005-printk-nbcon-Add-detailed-doc-for-write_atomic.patch
0006-printk-nbcon-Add-callbacks-to-synchronize-with-drive.patch
0007-printk-nbcon-Use-driver-synchronization-while-un-reg.patch
0008-serial-core-Provide-low-level-functions-to-lock-port.patch
0009-serial-core-Introduce-wrapper-to-set-uart_port-cons.patch
0010-console-Improve-console_srcu_read_flags-comments.patch
0011-nbcon-Add-API-to-acquire-context-for-non-printing-op.patch
0012-serial-core-Implement-processing-in-port-lock-wrappe.patch
0013-printk-nbcon-Do-not-rely-on-proxy-headers.patch
0014-printk-Make-console_is_usable-available-to-nbcon.patch
0015-printk-Let-console_is_usable-handle-nbcon.patch
0016-printk-Add-flags-argument-for-console_is_usable.patch
0017-printk-nbcon-Add-helper-to-assign-priority-based-on-.patch
0018-printk-nbcon-Provide-function-to-flush-using-write_a.patch
0019-printk-Track-registered-boot-consoles.patch
0020-printk-nbcon-Use-nbcon-consoles-in-console_flush_all.patch
0021-printk-nbcon-Add-unsafe-flushing-on-panic.patch
0022-printk-Avoid-console_lock-dance-if-no-legacy-or-boot.patch
0023-printk-Track-nbcon-consoles.patch
0024-printk-Coordinate-direct-printing-in-panic.patch
0025-printk-nbcon-Implement-emergency-sections.patch
0026-panic-Mark-emergency-section-in-warn.patch
0027-panic-Mark-emergency-section-in-oops.patch
0028-rcu-Mark-emergency-sections-in-rcu-stalls.patch
0029-lockdep-Mark-emergency-sections-in-lockdep-splats.patch
0031-printk-nbcon-Introduce-printing-kthreads.patch
0032-printk-Atomic-print-in-printk-context-on-shutdown.patch
0033-printk-nbcon-Fix-nbcon_cpu_emergency_flush-when-pree.patch
0034-printk-nbcon-Add-context-to-console_is_usable.patch
0035-printk-nbcon-Add-printer-thread-wakeups.patch
0036-printk-nbcon-Stop-threads-on-shutdown-reboot.patch
0037-printk-nbcon-Start-printing-threads.patch
0038-printk-Provide-helper-for-message-prepending.patch
0039-printk-nbcon-Show-replay-message-on-takeover.patch
0040-printk-Add-kthread-for-all-legacy-consoles.patch
0041-proc-consoles-Add-notation-to-c_start-c_stop.patch
0042-proc-Add-nbcon-support-for-proc-consoles.patch
0043-tty-sysfs-Add-nbcon-support-for-active.patch
0044-printk-Provide-threadprintk-boot-argument.patch
0045-printk-Avoid-false-positive-lockdep-report-for-legac.patch
0046-printk-nbcon-Add-function-for-printers-to-reacquire-.patch
0047-serial-8250-Switch-to-nbcon-console.patch
0048-serial-8250-Revert-drop-lockdep-annotation-from-seri.patch
#
prinkt-nbcon-Add-a-scheduling-point-to-nbcon_kthread.patch

###########################################################################
# Post
###########################################################################

###########################################################################
# Enabling
###########################################################################
x86__Allow_to_enable_RT.patch
x86__Enable_RT_also_on_32bit.patch
ARM64__Allow_to_enable_RT.patch
riscv-allow-to-enable-RT.patch

###########################################################################
# For later, not essencial
###########################################################################
# Posted
sched-rt-Don-t-try-push-tasks-if-there-are-none.patch

# sparse
0001-locking-rt-Add-sparse-annotation-PREEMPT_RT-s-sleepi.patch
0002-locking-rt-Remove-one-__cond_lock-in-RT-s-spin_trylo.patch
0003-locking-rt-Add-sparse-annotation-for-RCU.patch
0004-locking-rt-Annotate-unlock-followed-by-lock-for-spar.patch
0001-timers-Add-sparse-annotation-for-timer_sync_wait_run.patch
0002-hrtimer-Annotate-hrtimer_cpu_base_.-_expiry-for-spar.patch

# Needs discussion first.
softirq-Use-a-dedicated-thread-for-timer-wakeups.patch
rcutorture-Also-force-sched-priority-to-timersd-on-b.patch
tick-Fix-timer-storm-since-introduction-of-timersd.patch
softirq-Wake-ktimers-thread-also-in-softirq.patch

# zram
0001-zram-Replace-bit-spinlocks-with-a-spinlock_t.patch
0002-zram-Remove-ZRAM_LOCK.patch
0003-zram-Shrink-zram_table_entry-flags.patch

# Sched
0001-sched-core-Provide-a-method-to-check-if-a-task-is-PI.patch
0002-softirq-Add-function-to-preempt-serving-softirqs.patch
0003-time-Allow-to-preempt-after-a-callback.patch

###########################################################################
# DRM:
###########################################################################
# https://lore.kernel.org/all/20240613102818.4056866-1-bigeasy@linutronix.de/
0001-drm-i915-Use-preempt_disable-enable_rt-where-recomme.patch
0002-drm-i915-Don-t-disable-interrupts-on-PREEMPT_RT-duri.patch
0003-drm-i915-Don-t-check-for-atomic-context-on-PREEMPT_R.patch
0004-drm-i915-Disable-tracing-points-on-PREEMPT_RT.patch
0005-drm-i915-gt-Use-spin_lock_irq-instead-of-local_irq_d.patch
0006-drm-i915-Drop-the-irqs_disabled-check.patch
0007-drm-i915-guc-Consider-also-RCU-depth-in-busy-loop.patch
0008-Revert-drm-i915-Depend-on-PREEMPT_RT.patch

# Lazy preemption
PREEMPT_AUTO.patch

###########################################################################
# ARM
###########################################################################
0001-arm-Disable-jump-label-on-PREEMPT_RT.patch
ARM__enable_irq_in_translation_section_permission_fault_handlers.patch
arm-Disable-FAST_GUP-on-PREEMPT_RT-if-HIGHPTE-is-als.patch
0001-ARM-vfp-Provide-vfp_lock-for-VFP-locking.patch
0002-ARM-vfp-Use-vfp_lock-in-vfp_sync_hwstate.patch
0003-ARM-vfp-Use-vfp_lock-in-vfp_support_entry.patch
0004-ARM-vfp-Move-sending-signals-outside-of-vfp_lock-ed-.patch
ARM__Allow_to_enable_RT.patch

###########################################################################
# POWERPC
###########################################################################
powerpc__traps__Use_PREEMPT_RT.patch
powerpc_pseries_iommu__Use_a_locallock_instead_local_irq_save.patch
powerpc-pseries-Select-the-generic-memory-allocator.patch
powerpc_kvm__Disable_in-kernel_MPIC_emulation_for_PREEMPT_RT.patch
powerpc_stackprotector__work_around_stack-guard_init_from_atomic.patch
POWERPC__Allow_to_enable_RT.patch

###########################################################################
# RISC-V
###########################################################################
riscv-add-PREEMPT_AUTO-support.patch

# Sysfs file vs uname() -v
sysfs__Add__sys_kernel_realtime_entry.patch

###########################################################################
# RT release version
###########################################################################
Add_localversion_for_-RT_release.patch
